package com.test.junit.mockito.junit.mockito.test.service;

import com.test.junit.mockito.junit.mockito.test.component.UtilComponent;
import com.test.junit.mockito.junit.mockito.test.entity.PersonEntity;
import com.test.junit.mockito.junit.mockito.test.model.PersonModel;
import com.test.junit.mockito.junit.mockito.test.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class PersonServiceImpl implements PersonService {

  @Autowired
  private PersonRepository personRepository;
  
  @Autowired
  private UtilComponent utilComponent;

  @Override
  public PersonModel getPersonById(PersonModel personModel) {
    PersonModel personModel2 = null;

    PersonEntity personEntity = personRepository.findByPersonId(personModel.getPersonId());
    if (!Objects.isNull(personEntity)) {
      personModel2 = utilComponent.mapper(personEntity);
    }
    return personModel2;
  }

  @Override
  public List<PersonModel> getAllPersonIdSeparateForId() {
    List<PersonModel> listPersonModel = new ArrayList<>();
    
    List<PersonEntity> persons = personRepository.findAll();
    if(!Objects.isNull(persons)) {
      listPersonModel = persons.stream().map(x -> utilComponent.mapper(x)).collect(Collectors.toList());
    }
    
    return listPersonModel;
  }
}
